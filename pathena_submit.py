#! /usr/bin/env python

import os
from datetime import date

date = date.today().strftime("%d_%m_%y")
flag = "SET"

datasets = [
    ## The training set based on ttbar nonallhad only with extra slices in MET
    # ( "ttbar_410470", "mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_PHYS.e6337_s3126_r10724_p4355" ),
    # ( "ttbar_345935", "mc16_13TeV.345935.PhPy8EG_A14_ttbarMET100_200_hdamp258p75_nonallhad.deriv.DAOD_PHYS.e6620_s3126_r10724_p4355" ),
    # ( "ttbar_407345", "mc16_13TeV.407345.PhPy8EG_A14_ttbarMET200_300_hdamp258p75_nonallhad.deriv.DAOD_PHYS.e6414_s3126_r10724_p4355" ),
    # ( "ttbar_407346", "mc16_13TeV.407346.PhPy8EG_A14_ttbarMET300_400_hdamp258p75_nonallhad.deriv.DAOD_PHYS.e6414_s3126_r10724_p4355" ),
    # ( "ttbar_407347", "mc16_13TeV.407347.PhPy8EG_A14_ttbarMET400_hdamp258p75_nonallhad.deriv.DAOD_PHYS.e6414_s3126_r10724_p4355" ),

    ## ttbar allhad set to include in training if needed
    # ( "ttbar_410471", "mc16_13TeV.410471.PhPy8EG_A14_ttbar_hdamp258p75_allhad.deriv.DAOD_PHYS.e6337_s3126_r10724_p4355" ),

    ## Diboson set to include in training if needed
    # ( "llvv_364254", "mc16_13TeV.364254.Sherpa_222_NNPDF30NNLO_llvv.deriv.DAOD_PHYS.e5916_s3126_r10724_p4355" ),

    ## The ttbar testing sets
    # ( "ttbar_410470a", "mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_PHYS.e6337_s3126_r9364_p4355" ),
    # ( "ttbar_410470d", "mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_PHYS.e6337_s3126_r10201_p4355" ),
    # ( "shrptt_410252", "mc16_13TeV.410252.Sherpa_221_NNPDF30NNLO_ttbar_dilepton_MEPS_NLO.deriv.DAOD_TOPQ1.e5450_s3126_r10201_p3629" ),
    # ( "fastsim_ttbar", "mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_PHYS.e6337_a875_r9364_p4355" ),

    ## High MET SUSY testing samples
    # ( "susy_436073", "mc16_13TeV.436073.MGPy8EG_A14N23LO_TT_directTT_400_1.deriv.DAOD_SUSY1.e6985_a875_r10724_p3965" ),

    ## Other standard testing samples
    # ( "Zmumu_361107", "mc16_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.deriv.DAOD_JETM3.e3601_s3126_r10724_p4288" ),
    # ( "WW_361600", "mc16_13TeV.361600.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WWlvlv.deriv.DAOD_PHYS.e4616_s3126_r10724_p4355" ),
    # ( "ZZ_361604", "mc16_13TeV.361604.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZvvll_mll4.deriv.DAOD_PHYS.e4475_s3126_r10724_p4355" ),
    # ( "HW_345948", "mc16_13TeV.345948.PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_WWlvlv.deriv.DAOD_JETM3.e7172_s3126_r10724_p4062" ),
    ( "HZ_346600", "mc16_13TeV.346600.PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_ZZ4nu_MET75.deriv.DAOD_PHYS.e7613_s3126_r10724_p4355" ),
]

extra_files = "SUSYTools_Custom.conf"
output_files = "train-sample.csv"

for (output, input) in datasets:

    if "InputLists" in input:
        inDS = " --inDsTxt=%s" % ( input )
    else:
        inDS = " --inDS=%s" % ( input )

    outDS = " --outDS=user.mleigh.%s.%s.%s" % ( date, flag, output )
    extFiles = " --extFile=%s" % ( extra_files )
    extOutFile = " --extOutFile=%s" % ( output_files )
    command = "pathena" + inDS + outDS + extFiles + extOutFile + " --nGBPerJob=15 METTrainBuilder/METTrainBuilderAlgJobOptions.py"

    print ""
    print command
    os.system(command)
    print ""
