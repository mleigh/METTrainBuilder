
# Configure to run on data 0, or MC 1
is_MC = 1

## Use the name of some random input datafile (from the atlas software tutorial)
file_name = "../ttbar_sample.root"
jps.AthenaCommonFlags.FilesInput = [file_name]
jps.AthenaCommonFlags.HistOutputs = ["ANALYSIS:MyxAODAnalysis.outputs.root"]

## The read mode of the package
jps.AthenaCommonFlags.AccessMode = "ClassAccess"

## Add the algorithm to the list applied to each file
athAlgSeq += CfgMgr.METTrainBuilderAlg( "METTrainBuilder", OutputFileName = "train-sample.csv" )

## The MET Network tool (replacing metmaker)
metNet = CfgMgr.met__METNet( "METNetTool",
                              JetJvtMomentName = "Jvt",
                              JetFwdJvtMomentName = "fJvt",
                              JetFwdJvtCutName = "passFJvt" )
ToolSvc += metNet

## Adding SUSYTools to the config using the correct file
ToolSvc += CfgMgr.ST__SUSYObjDef_xAOD( "SUSYTools", METMaker = metNet )
config_file = "../SUSYTools_Custom.conf"
ToolSvc.SUSYTools.ConfigFile = config_file

## Set the maximum number of events processed (-1=All)
theApp.EvtMax = -1

## Let SUSYTools know the data type
ToolSvc.SUSYTools.DataSource = is_MC

## Pilup Reweighting for SUSYTools 2017
ToolSvc.SUSYTools.PRWLumiCalcFiles = ["GoodRunsLists/data18_13TeV/20190318/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root"]
ToolSvc.SUSYTools.AutoconfigurePRWTool = True

## Optional suppress as much athena output as possible.
include("AthAnalysisBaseComps/SuppressLogging.py")
