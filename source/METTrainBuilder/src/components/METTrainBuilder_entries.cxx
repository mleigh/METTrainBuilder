
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../METTrainBuilderAlg.h"

DECLARE_ALGORITHM_FACTORY( METTrainBuilderAlg )

DECLARE_FACTORY_ENTRIES( METTrainBuilder ) 
{
  DECLARE_ALGORITHM( METTrainBuilderAlg );
}
