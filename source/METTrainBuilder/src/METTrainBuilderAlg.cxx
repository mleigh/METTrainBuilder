// METTrainBuilder includes
#include "METTrainBuilderAlg.h"

// STL includes
#include <iostream>
#include <fstream>

// Physics objects
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODJet/JetContainer.h"

// Etmiss
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODMissingET/MissingETComposition.h"
#include "xAODMissingET/MissingETAssociationMap.h"

// Extra libraries
#include "xAODCore/ShallowCopy.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODBase/IParticleHelpers.h"

// ROOT
#include <TH1.h>

using std::string;
using namespace xAOD;

// Decorator for passing the vector of inputs to the met object
static const SG::AuxElement::ConstAccessor< std::vector<float> > acc_inputvalues("input_values");
static const SG::AuxElement::ConstAccessor< std::vector<std::string> > acc_inputnames("input_names");

//////////////////////////////////////////////////////////////////////////////

METTrainBuilderAlg::METTrainBuilderAlg( const std::string& name, ISvcLocator* pSvcLocator )
: ::AthAnalysisAlgorithm( name, pSvcLocator ) {

    declareProperty( "OutputFileName", m_outfilename = "Training_Dataset.csv" );

}

//////////////////////////////////////////////////////////////////////////////

METTrainBuilderAlg::~METTrainBuilderAlg() {}

//////////////////////////////////////////////////////////////////////////////

StatusCode METTrainBuilderAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");

  // Initialise and retrieve SUSYTools
  m_SUSYTools.setTypeAndName("ST::SUSYObjDef_xAOD/SUSYTools");
  ATH_CHECK( m_SUSYTools.retrieve() );

  // Opening the output file
  m_firstLine = true;
  output_file.open( m_outfilename );
  output_file << std::setprecision(8);

  // Setting up the histogram
  ATH_CHECK(book(TH1F("h_TruePtMiss", "h_TruePtMiss", 200, 0, 450)));

  return StatusCode::SUCCESS;
}

//////////////////////////////////////////////////////////////////////////////

StatusCode METTrainBuilderAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");

  // Closing the output file
  output_file.close();

  return StatusCode::SUCCESS;
}

//////////////////////////////////////////////////////////////////////////////

StatusCode METTrainBuilderAlg::execute() {
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  setFilterPassed(false);

  // SUSYTools requires the PRW tool to be applied first (needed in many calculations)
  ATH_CHECK( m_SUSYTools->ApplyPRWTool() );

  // METMaker crashes if the event has no PV, we check this first!
  if ( m_SUSYTools->GetPrimVtx() == nullptr ) {
    ATH_MSG_WARNING( "No PV found for this event! Skipping..." );
    return StatusCode::SUCCESS;
  }

  // We retrieve the event information
  const EventInfo* ei = 0;
  ATH_CHECK( evtStore()->retrieve( ei, "EventInfo" ) );
  long int ChannelNumber = ei->mcChannelNumber();

  // ATLAS Event cleaning
  if ( ei->errorState(xAOD::EventInfo::LAr)  == xAOD::EventInfo::Error ) return StatusCode::SUCCESS;
  if ( ei->errorState(xAOD::EventInfo::Tile) == xAOD::EventInfo::Error ) return StatusCode::SUCCESS;
  if ( ei->errorState(xAOD::EventInfo::SCT)  == xAOD::EventInfo::Error ) return StatusCode::SUCCESS;

  // The object containers are retreived, calibrated, decorated (baseline,signal...)
  MuonContainer* muons = 0;
  ShallowAuxContainer* muons_aux = 0;
  ATH_CHECK( m_SUSYTools->GetMuons( muons, muons_aux, true ) );

  ElectronContainer* electrons = 0;
  ShallowAuxContainer* electrons_aux = 0;
  ATH_CHECK( m_SUSYTools->GetElectrons( electrons, electrons_aux, true ) );

  PhotonContainer* photons = 0;
  ShallowAuxContainer* photons_aux = 0;
  ATH_CHECK( m_SUSYTools->GetPhotons( photons, photons_aux, true ) );

  JetContainer* jets = 0;
  ShallowAuxContainer* jets_aux = 0;
  ATH_CHECK( m_SUSYTools->GetJets( jets, jets_aux, true ) );

  // Calculate the MET of the event using SUSYTools instance of METMaker
  auto myMetCont = std::make_shared<MissingETContainer>();
  auto myMetAuxCont = std::make_shared<MissingETAuxContainer>();
  myMetCont->setStore( myMetAuxCont.get() );
  ATH_CHECK( m_SUSYTools->GetMET( *myMetCont, jets, electrons, muons, photons, 0, true, true ) );

  // Collecting and saving the Truth MET and associated components
  const xAOD::MissingETContainer* TruthMet = 0;
  ATH_CHECK( evtStore()->retrieve( TruthMet, "MET_Truth" ) );

  // Saving network inputs+targets+dsids to the csv file (names on first line only)
  if (m_firstLine) {
    m_firstLine = false;

    std::vector<std::string> net_names = acc_inputnames(*(*myMetCont)["Net_Inputs"]);
    for ( auto& name : net_names ) { output_file << name << ','; }
    output_file << "True_ET,True_EX,True_EY,DSID\n";
  }

  // We discard the event if the Tight SumET is too large. This only removes ~10 events but improves training!
  float Tight_SumET = acc_inputvalues(*(*myMetCont)["Net_Inputs"])[3];
  if ( Tight_SumET > 3e6 ) return StatusCode::SUCCESS;

  // Fill in a histogram containing the true MET variables
  hist("h_TruePtMiss")->Fill((*TruthMet)["NonInt"]->met() * 0.001); // GeV

  // Iterate through the input values vector and save to the csv file
  std::vector<float> net_inputs = acc_inputvalues(*(*myMetCont)["Net_Inputs"]);
  for ( auto& inpt : net_inputs ) { output_file << inpt << ','; }
  output_file << (*TruthMet)["NonInt"]->met() << ','
              << (*TruthMet)["NonInt"]->mpx() << ','
              << (*TruthMet)["NonInt"]->mpy() << ','
              << ChannelNumber << '\n';

  setFilterPassed(true);
  return StatusCode::SUCCESS;
}

//////////////////////////////////////////////////////////////////////////////
