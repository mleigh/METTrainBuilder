#ifndef METTRAINBUILDER_METTRAINBUILDERALG_H
#define METTRAINBUILDER_METTRAINBUILDERALG_H

// Base class includes
#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"

// STL includes
#include <iostream>
#include <fstream>

// Tool includes
#include "SUSYTools/ISUSYObjDef_xAODTool.h"

class METTrainBuilderAlg: public ::AthAnalysisAlgorithm {

 public:

  METTrainBuilderAlg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~METTrainBuilderAlg();

  virtual StatusCode  initialize();
  virtual StatusCode  execute();
  virtual StatusCode  finalize();

 private:

   // Class properties
   std::string m_outfilename;
   std::ofstream output_file;

   // Class attributes
   bool m_firstLine;

   // Tool handles
   ToolHandle<ST::ISUSYObjDef_xAODTool> m_SUSYTools;


};

#endif
