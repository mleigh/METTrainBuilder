setupATLAS
mkdir -p build run
cd build

acmSetup AthAnalysis,21.2,latest
acm sparse_clone_project athena mleigh/athena
acm add_pkg athena/Reconstruction/MET/METUtilities
acm compile

cd ../run


